/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    AppFooter: typeof import('./components/AppFooter.vue')['default']
    CloudChamber: typeof import('./components/CloudChamber.vue')['default']
    CMSEventDisplay: typeof import('./components/CMSEventDisplay.vue')['default']
    LHCInfo: typeof import('./components/LHCInfo.vue')['default']
    LHCInfo2: typeof import('./components/LHCInfo2.vue')['default']
    LiveStream: typeof import('./components/LiveStream.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    SlideShow: typeof import('./components/SlideShow.vue')['default']
    SplitPanel: typeof import('./components/SplitPanel.vue')['default']
  }
}
