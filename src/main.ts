/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'
import keycloak from '@/services/Keycloak'

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

const app = createApp(App)

registerPlugins(app)

app.provide('keycloak', keycloak)

keycloak
    .init({ onLoad: 'login-required' })
    .then((auth) => {
        if (!auth) {
            window.location.reload()
        }
        app.mount('#app')
    })
    .catch((e) => {
        console.error('Authentication failed', e)
    })


