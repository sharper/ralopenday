import Keycloak from 'keycloak-js'
import type { KeycloakConfig } from 'keycloak-js'

const initOptions: KeycloakConfig = {
    url: 'https://auth.cern.ch/auth',
    realm: 'cern',
    clientId: 'cms-tsg-frontend-client'
}

const keycloak: Keycloak = new Keycloak(initOptions)
export default keycloak
