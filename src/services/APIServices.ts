import axios from 'axios'
import qs from 'qs'
import type { ResultDisplaySelection } from '@/types.ts'
const apiClient = axios.create({
    baseURL: "https://hltsupervisor.app.cern.ch/", //import.meta.env.VITE_APP_API_BASE_URL,
    withCredentials: false, // This is the default
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'

      }
})
import keycloak from '@/services/Keycloak'
async function updateToken(): Promise<string> {
    return keycloak
        .updateToken(70)
        .then(() => {
            return keycloak.token as string
        })
        .catch(() => {
            return keycloak.token as string
        })
}

let intervalID: undefined | ReturnType<typeof setTimeout>

apiClient.interceptors.response.use(async function (response) {
    if (response.status == 202) {
        let pollingResponse = await apiClient.get(response.headers.location)

        await new Promise<void>((resolve, reject) => {
            intervalID = setInterval(async function () {
                try {
                    pollingResponse = await apiClient.get(response.headers.location)
                } catch (e) {
                    clearInterval(intervalID)
                    console.log('error is here', e)
                    reject(e)
                }
                if (pollingResponse.data.status === 'SUCCESS') {
                    clearInterval(intervalID)
                    resolve()
                } else if (pollingResponse.data.status === 'FAILURE') {
                    clearInterval(intervalID)
                    reject(pollingResponse.data)
                }
            }, 3000)
        })

        if (pollingResponse.data.status === 'FAILURE') {
            throw 'Operation failed!'
        } else {
            return await apiClient.get(pollingResponse.headers.resourcelocation)
        }
    } else {
        return response
    }
})

apiClient.interceptors.request.use(async (config) => {
    const token = await updateToken()
    config.headers['Authorization'] = `Bearer ${token}`
    return config
})

export default {
    getRunData(runNumber : number | undefined) {
      let omsQuery="agg/api/v1/runs?fields=run_number,last_lumisection_number,fill_number,end_time,last_update,tier0_transfer,l1_hlt_mode_stripped,hlt_key&page[offset]=0&page[limit]=1&filter[sequence][EQ]=GLOBAL-RUN&sort=-run_number"
  
      if(runNumber!=undefined){
        omsQuery+=`&filter[run_number][EQ]=${runNumber}`
      }
      
      return apiClient.get("api/v0/oms/"+omsQuery)
    },
    getDatasetRates(runNumber : number,lumiSec : number | undefined){
      const lumiSecForQuery : string = lumiSec===undefined ? "last" : lumiSec.toString()
      const omsQuery=`agg/api/v1/datasetrates?fields=last_lumisection_number,dataset_name,rate,events&page[offset]=0&page[limit]=1000&filter[last_lumisection_number][EQ]=${lumiSecForQuery}&filter[run_number][EQ]=${runNumber}&sort=-last_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`    
      //const omsQuery=`agg/api/v1/datasetrates?fields=last_lumisection_number,dataset_name,rate,events&page[offset]=0&page[limit]=1000&filter[last_lumisection_number][EQ]=${lumiSec}&filter[run_number][EQ]=${runNumber}&sort=-last_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`    
      return apiClient.get("api/v0/oms/"+omsQuery)  
    },
    getStreamRates(runNumber : number,lumiSec : number | undefined){
      const lumiSecForQuery : string = lumiSec===undefined ? "last" : lumiSec.toString()
      const omsQuery=`agg/api/v1/streams?fields=last_lumisection_number,stream_name,rate,bandwidth,n_events&page[offset]=0&page[limit]=100&filter[last_lumisection_number][EQ]=${lumiSecForQuery}&filter[run_number][EQ]=${runNumber}&sort=-last_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`
      //const omsQuery=`f3mon/api/v1/streams?fields=last_lumisection_number,stream_name,rate,bandwidth,n_events&page[offset]=0&page[limit]=100&filter[last_lumisection_number][EQ]=last&filter[run_number][EQ]=${runNumber}&sort=-last_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`
      //const omsQuery=`agg/api/v1/streams?fields=last_lumisection_number,stream_name,rate,bandwidth,n_events&page[offset]=0&page[limit]=100&filter[last_lumisection_number][EQ]=${lumiSec}&filter[run_number][EQ]=${runNumber}&sort=-last_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`    
      return apiClient.get("api/v0/oms/"+omsQuery)  
    },  
    getStreamRatesF3mon(runNumber : number,lumiSec : number | undefined){
      const lumiSecFilter : string = lumiSec===undefined ? "filter[last_ls][EQ]=true" : `filter[lumisection_number][EQ]=${lumiSec.toString()}`   
      const omsQuery=`f3mon/api/v1/streams?filter[run_number][EQ]=${runNumber}&${lumiSecFilter}`
      return apiClient.get("api/v0/oms/"+omsQuery)  
    },
    getLumiSection(runNumber : number, lumiSec : number | undefined){
      const lumiSecFilter : string = lumiSec===undefined ? `filter[lumisection_number][GT]=-1` : `filter[lumisection_number][EQ]=${lumiSec.toString()}`
      const omsQuery=`agg/api/v1/lumisections?fields=lumisection_number,init_lumi,pileup&page[offset]=0&page[limit]=1&filter[cms_active][EQ]=true&filter[run_number][EQ]=${runNumber}&${lumiSecFilter}&sort=-lumisection_number&include=meta,presentation_timestamp`
      return apiClient.get("api/v0/oms/"+omsQuery)  
    },
    getL1Rates(runNumber: number, lumiSec : number | undefined){
      const lumiSecFilter : string = lumiSec===undefined ? `filter[lumisection_number][GT]=-1` : `filter[lumisection_number][EQ]=${lumiSec.toString()}`  
      const omsQuery=`agg/api/v1/l1triggerrates?page[offset]=0&page[limit]=1&filter[run_number][EQ]=${runNumber}&${lumiSecFilter}&group[granularity]=lumisection&fields=last_lumisection_number,l1a_total,l1a_random&sort=-lumisection_number`
      return apiClient.get("api/v0/oms/"+omsQuery)
    },
    getFillInfo(fillNumber: number){
      const omsQuery=`agg/api/v1/fills?fields=fill_number,bunches_target,fill_type_runtime&page[offset]=0&page[limit]=1&filter[fill_number][EQ]=${fillNumber}&sort=-fill_number`
      return apiClient.get("api/v0/oms/"+omsQuery)
    },
    getDisksStatus(runNumber: number){
      const f3monQuery = `f3mon/api/getDisksStatus?&runNumber=${runNumber}&sysName=cdaq`
      return apiClient.get("api/v0/daq/"+f3monQuery)
    },
    getDeadtime(runNumber: number, lumiSec : number | undefined){
      const lumiSecFilter : string = lumiSec===undefined ? `filter[first_lumisection_number][GT]=-1` : `filter[first_lumisection_number][EQ]=${lumiSec.toString()}`
      const omsQuery=`agg/api/v1/deadtimes?fields=overall_total_deadtime,beamactive_total_deadtime,first_lumisection_number&page[offset]=0&page[limit]=1&filter[run_number][EQ]=${runNumber}&${lumiSecFilter}&sort=-first_lumisection_number&include=meta,presentation_timestamp&group[granularity]=lumisection`
      return apiClient.get("api/v0/oms/"+omsQuery)  
    },
    getPSChanges(runNumber: number){
      //returns reverse sort of lumisections thus the first element is the last change
      const omsQuery=`agg/api/v1/prescalechanges?&page[offset]=0&page[limit]=1000&filter[run_number][EQ]=${runNumber}&sort=-lumisection_number&include=meta,presentation_timestamp`
      return apiClient.get("api/v0/oms/"+omsQuery)  
    }, 
    getCPUUsage(){
      const daqQuery = `sc/php/cpuusage.php`
      return apiClient.get("api/v0/daq/"+daqQuery)  
    },
    getThresholds(){
      return apiClient.get("api/v0/thresholds")
    },
    getThresholdHistory(){
      return apiClient.get("api/v0/thresholds/history")
    },
    setThresholds(payload : {streams:Array<RateLimit>,datasets:Array<RateLimit>}){        
      return apiClient.post(`api/v0/thresholds`,{"thresholds" : payload})
    },
    
  }
  